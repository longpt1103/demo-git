var Dog = require('./Dog');
var Cat = require('./Cat');
var Mouse = require('./Mouse');
var Tommy  = new Dog('Tommy');
var Tom  = new Cat('Tom');
var Jerry  = new Mouse('Jerry');

Tom.eat(Jerry);
console.log(Tom);
